﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using EvaluationServiceApp.Model;

namespace EvaluationServiceApp
{
    public class ReturnResult
    {
        public bool isError { get; set; }
        public string result { get; set; }
        public string errorMessage { get; set; }
    }

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public ReturnResult GetIdReviewer(string processId)
        {
            using (var db = new EvaluationDbDataContext())
            {
                var dataRequest = db.Makers
                    .Where(o => o.ProcessId == Int32.Parse(processId)).SingleOrDefault();
                if (dataRequest != null)
                {
                    return new ReturnResult()
                    {
                        isError = false,
                        result = dataRequest.EvaluationBy,
                        errorMessage = ""
                    };
                }
                else
                {
                    return new ReturnResult()
                    {
                        isError = true,
                        result = "",
                        errorMessage = "Process Id Not Found"

                    };
                }
            }
        }

        public ReturnResult GetIdManager(string processId)
        {
            using(var db = new EvaluationDbDataContext())
            {
                var dataRequest = db.Makers.Where(o => o.ProcessId == Int32.Parse(processId)).SingleOrDefault();
                if (dataRequest != null)
                {
                    var dataUser = db.Users.Where(o => o.ID == dataRequest.EvaluationBy).SingleOrDefault();
                    if(dataUser!=null)
                    {
                        return new ReturnResult()
                        {
                            isError = false,
                            result = dataUser.ReportingLineID,
                            errorMessage = ""
                        };
                    }
                    else
                    {
                        return new ReturnResult()
                        {
                            isError=true,
                            result="",
                            errorMessage="User"+dataRequest.EvaluationBy+" Not Found"
                        };
                    }

                }
                else
                {
                    return new ReturnResult()
                    {
                        isError = true,
                        result = "",
                        errorMessage = "Process Id Not Found"

                    };
                }
            }

            
        }

        public ReturnResult GetIdSeniorManager(string processId)
        {
            using (var db = new EvaluationDbDataContext())
            {
                var dataRequest = db.Makers.Where(o => o.ProcessId == Int32.Parse(processId)).SingleOrDefault();
                if (dataRequest != null)
                {
                    var dataUser = db.Users.Where(o => o.ID == dataRequest.EvaluationBy).SingleOrDefault();
                    if (dataUser != null)
                    {

                        var dataUserSenior = db.Users.Where(o => o.ID == dataUser.ReportingLineID).SingleOrDefault();
                        if (dataUserSenior != null)
                        {
                            return new ReturnResult()
                            {
                                isError = false,
                                result = dataUserSenior.ReportingLineID,
                                errorMessage = ""
                            };
                        }
                        else
                        {
                            return new ReturnResult()
                            {
                                isError = true,
                                result = "",
                                errorMessage = "User" + dataRequest.EvaluationBy + " Not Found"
                            };
                        }
                        
                    }
                    else
                    {
                        return new ReturnResult()
                        {
                            isError = true,
                            result = "",
                            errorMessage = "User" + dataRequest.EvaluationBy + " Not Found"
                        };
                    }

                }
                else
                {
                    return new ReturnResult()
                    {
                        isError = true,
                        result = "",
                        errorMessage = "Process Id Not Found"

                    };
                }
            }
        }


    }
}
